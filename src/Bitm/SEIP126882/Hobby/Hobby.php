<?php
namespace App\Bitm\SEIP126882\Hobby;
use App\Bitm\SEIP126882\Utility\Utility;
use App\Bitm\SEIP126882\Message\Message;
class Hobby{
    public $id="";
    public $name="";
    public $lastName="";
    public $hobby="";



    public function prepare($data=Array()){
        if(array_key_exists("name",$data)){
            $this->name= $data['name'];
        }
        if(array_key_exists("lastname",$data)){
            $this->lastName= $data['lastname'];
        }
        if(array_key_exists("hobby",$data)){
            $this->hobby= $data['hobby'];
        }
        if(array_key_exists("id",$data)){
            $this->id= $data['id'];
        }

         //Utility::dd($this->name);
        return $this;

    }
    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","atomicprojectb21") or die("Database connection failed");
    }

    public function store(){
        $query="INSERT INTO `atomicprojectb21`.`hobby` ( `name`, `lastname`, `hobbies`) VALUES ('{$this->name}','{$this->lastName}','{$this->hobby}')";
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been stored successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }
    public function index(){
        $_allHobby= array();
        $query="SELECT * FROM `hobby`";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allHobby[]=$row;
        }

        return  $_allHobby;
    }
    public function view(){
        $query="SELECT * FROM `hobby` WHERE `id`=".$this->id;

        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }


}