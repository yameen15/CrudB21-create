<?php
namespace App\Bitm\SEIP126882\Book;
use App\Bitm\SEIP126882\Message\Message;

class Book{
    public $id="";
    public $title="";
    public $description="";
    public $filterByTitle="";
    public $filterByDescription="";
    public $search="";
    public $conn;
    public $deleted_at;


    public function prepare($data=""){
        if(array_key_exists("title",$data)){
            $this->title=  filter_var($data['title'], FILTER_SANITIZE_STRING);
        }
        if(array_key_exists("description",$data)){
            $this->description=  $data['description'];
        }
        if(array_key_exists("filterByTitle",$data)){
            $this->filterByTitle=  $data['filterByTitle'];
        }
        if(array_key_exists("filterByDescription",$data)){
            $this->filterByDescription=  $data['filterByDescription'];
        }
        if(array_key_exists("search",$data)){
            $this->search=  $data['search'];
        }
        if(array_key_exists("id",$data)){
            $this->id= $data['id'];
        }

        return $this;

    }

    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","atomicprojectb21") or die("Database connection failed");
    }


    public function store(){
        $query="INSERT INTO `atomicprojectb21`.`book` (`title`, `description`) VALUES ('".$this->title."', '".$this->description."')";
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been stored successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }

    public function index(){
        $whereClause=" 1=1 ";
        if(!empty($this->filterByTitle)) {
            $whereClause .= " AND title LIKE '%".$this->filterByTitle."%'";
        }
        if(!empty($this->filterByDescription)) {
            $whereClause .= " AND description LIKE '%".$this->filterByDescription."%'";
        }
        if(!empty($this->search)) {
            $whereClause .= " AND description LIKE '%".$this->search."%' OR title LIKE '%".$this->search."%'";
        }
        $_allBook= array();
        $query="SELECT * FROM `book` WHERE `deleted_at` IS NULL AND ".$whereClause;
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allBook[]=$row;
        }

        return $_allBook;
    }


    public function view(){
        $query="SELECT * FROM `book` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }

    public function update(){
        $query="UPDATE `atomicprojectb21`.`book` SET `title` = '".$this->title."' WHERE `book`.`id` =".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Updated successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `atomicprojectb21`.`book` WHERE `book`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }

    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectb21`.`book` SET `deleted_at` = '".$this->deleted_at."' WHERE `book`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }



    }


    public function trashed(){
        $_trashedBook= array();
        $query="SELECT * FROM `book` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_trashedBook[]=$row;
        }

        return $_trashedBook;

    }

    public function recover(){
        $query="UPDATE `atomicprojectb21`.`book` SET `deleted_at` = NULL  WHERE `book`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }

        }
    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicprojectb21`.`book` SET `deleted_at` = NULL  WHERE `book`.`id` IN(".$IDs.")";
            //result= mysqli_query($this->conn,$query);
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }



        }

    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `book`";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];

    }

    public function paginator($pageStartFrom=0,$Limit=5){
        $query="SELECT * FROM `book` LIMIT ".$pageStartFrom.",".$Limit;
        $_allBook= array();
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allBook[]=$row;
        }

        return $_allBook;

    }
    public function getAllTitle(){
        $_allBook= array();
        $query="SELECT title FROM `book`";
        $result=mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $_allBook[]=$row['title'];
        }

        return $_allBook;
    }





}