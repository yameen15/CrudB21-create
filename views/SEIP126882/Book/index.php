<?php
session_start();
include_once('../../../vendor/autoload.php.');
use App\Bitm\SEIP126882\Book\Book;
use App\Bitm\SEIP126882\Utility\Utility;
use App\Bitm\SEIP126882\Message\Message;


$book= new Book();
$allTitle=$book->getAllTitle();
//Utility::dd($allTitle);
$comma_separated= '"'.implode('","',$allTitle).'"';

//Utility::d($allBook);
$totalItem=$book->count();
//Utility::dd($totalItem);
if(array_key_exists('itemPerPage',$_SESSION)){
  if(array_key_exists('itemPerPage',$_GET)){
    $_SESSION['itemPerPage']=$_GET['itemPerPage'];
  }
}
  else{
    $_SESSION['itemPerPage']=5;
  }

$itemPerPage= $_SESSION['itemPerPage'];


$noOfPage= ceil($totalItem/$itemPerPage);
//Utility::d($noOfPage);
$pagination="";
if(array_key_exists('pageNumber',$_GET)){
  $pageNo=$_GET['pageNumber'];
}
else{
  $pageNo=1;
}
for($i=1;$i<=$noOfPage;$i++){
  $active=($pageNo==$i)?"active":"";
  $pagination.="<li class='$active'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNo-1);

if(strtoupper($_SERVER['REQUEST_METHOD']=="GET")){
$allBook=$book->paginator($pageStartFrom,$itemPerPage);
}
if(strtoupper($_SERVER['REQUEST_METHOD']=="POST")){
  $allBook=$book->prepare($_POST)->index();
}
if((strtoupper($_SERVER['REQUEST_METHOD']=="GET"))&& isset($_GET['search'])){
  $allBook=$book->prepare($_GET)->index();
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
<!--  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
</head>
<body>

<div class="container">
  <h2>Book List</h2>

  <a href="create.php" class="btn btn-info" role="button">Add Book Title</a>
  <a href="trashed_view.php" class="btn btn-primary" role="button">Trashed List</a><br><br>
  <a href="pdf.php" class="btn btn-info" role="button">Download as PDF</a>
  <a href="xl.php" class="btn btn-info" role="button">Download as XL</a>
  <a href="mail.php" class="btn btn-info" role="button">Email to friend</a>

  <br>
  <div id="message">

  <?php if((array_key_exists('message',$_SESSION)&& !empty($_SESSION['message']))){
    echo Message::message();
  }?>
  </div>
  <form role="form" action="index.php">
    <div class="form-group">
      <label for="sel1">Select list (select one):</label>
      <select class="form-control" id="sel1" name="itemPerPage">
        <option>5</option>
        <option>10</option>
        <option selected>15</option>
        <option>20</option>
        <option>25</option>
      </select>
      <button type="submit">Go!</button>

  </form>
  <form role="form" action="index.php" method="post">
    <div class="form-group">
      <label>Filter by Title:</label>
      <input type="text" name="filterByTitle" value="" id="title">
      <label>Filter by Description:</label>
      <input type="text" name="filterByDescription" value="">
      <button type="submit">Fileter!</button>
  </form>
  <form role="form" action="index.php" method="get">
    <div class="form-group">
      <label>search:</label>
      <input type="text" name="search" value="">
      <button type="submit">Search!</button>
  </form>
    <div class="table-responsive">
  <table class="table">
    <thead>
      <tr>
        <th>SL#</th>
        <th>ID</th>
        <th>Book Title</th>
        <th>Description</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $sl=0;
    foreach($allBook as $book){
        $sl++;
        ?>
      <tr>
        <td><?php echo $sl+$pageStartFrom; ?></td>
        <td><?php echo $book['id'] // for object: $book->id ; ?></td>
        <td><?php echo $book['title'] // for object: $book->title; ?></td>
        <td><?php echo $book['description'] ?> </td>
        <td>
            <a href="view.php?id=<?php echo $book['id']?>" class="btn btn-info  btn-xs" role="button">View</a>
            <a href="edit.php?id=<?php echo $book['id']?>" class="btn btn-primary  btn-xs" role="button">Edit</a>
            <a href="delete.php?id=<?php echo $book['id']?>" class="btn btn-danger  btn-xs" role="button">Delete</a>
            <a href="trash.php?id=<?php echo $book['id']?>" class="btn btn-info  btn-xs" role="button">Trash</a>
        </td>


      </tr>
    <?php } ?>


    </tbody>
  </table>
      <?php if(strtoupper($_SERVER['REQUEST_METHOD']=="GET")){?>
      <ul class="pagination">
        <li><a href="#">Prev</a></li>
        <?php echo $pagination?>
        <li><a href="#">Next</a></li>
      </ul>
      <?php }?>
  </div>
</div>
<script>
  $('#message').show().delay(3000).fadeOut();
</script>
<script>
  $( function() {
    var availableTags = [
      <?php echo $comma_separated?>
    ];
    $( "#title" ).autocomplete({
      source: availableTags
    });
  } );
</script>

</body>
</html>
