<!DOCTYPE html>
<html lang="en">
<head>
    <title>Book</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/js/bootstrap.js">
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
</head>
<body>

<div class="container">
    <h2>Atomic Project- Book</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label>Enter Book title:</label>
            <input type="text" name="title" class="form-control" id="email" placeholder="Enter book title">
        </div>
        <div class="form-group">
            <label>Enter book description:</label>
            <textarea class="form-control" rows="5" id="description" name="description"></textarea>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
<script>
    tinymce.init({
        selector: '#description'
    });
</script>

</body>
</html>
